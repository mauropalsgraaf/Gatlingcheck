package org.mauropalsgraaf.gatling

import io.circe.Encoder
import io.circe.generic.semiauto._
import io.gatling.commons.validation.Success
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder
import org.mauropalsgraaf.gatling.Syntax._
import org.scalacheck.Gen
import org.scalacheck.Gen._

class Example extends Simulation {
  case class Person(firstName: String, lastName: String, age: Int)

  implicit val personEncoder: Encoder[Person] = deriveEncoder[Person]

  implicit val personGen: Gen[Person] =
    for {
      firstName <- alphaStr
      lastName <- alphaStr
      age <- posNum[Int]
    } yield Person(firstName, lastName, age)

  val request: HttpRequestBuilder = http(_ => Success("name"))
        .get(org.asynchttpclient.uri.Uri.create("https://google.com/"))
        .genBodyUnsafe[Person]
}

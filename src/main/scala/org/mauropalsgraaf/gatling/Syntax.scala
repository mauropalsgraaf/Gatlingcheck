package org.mauropalsgraaf.gatling

import io.circe.Encoder
import io.gatling.commons.validation.Success
import io.gatling.core.body.StringBody
import io.gatling.core.config.GatlingConfiguration
import org.scalacheck.Gen
import io.gatling.http.request.builder.HttpRequestBuilder
import org.scalacheck.Gen.Parameters.default
import org.scalacheck.rng.Seed

object Syntax {
  implicit class GatlingHttpOps(builder: HttpRequestBuilder) {
    def genBody[T](generatorParamaters: Gen.Parameters, seed: Seed)(implicit gen: Gen[T], encoder: Encoder[T], config: GatlingConfiguration): Option[HttpRequestBuilder] = {
      val generatedValue = gen(generatorParamaters, seed)
      generatedValue map {
        v => builder.body(
          StringBody(
            _ => Success(encoder(v).toString())
          )
        ).asJSON
      }
    }

    def genBody[T](implicit gen: Gen[T], encoder: Encoder[T], config: GatlingConfiguration): Option[HttpRequestBuilder] = genBody(default,Seed.random())

    def genBodyUnsafe[T](implicit gen: Gen[T], encoder: Encoder[T], config: GatlingConfiguration): HttpRequestBuilder = genBody.get
  }
}

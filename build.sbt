name := "Gatlingcheck"

version := "0.1"

scalaVersion := "2.12.4"

val circeVersion = "0.9.1"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "org.scalacheck" %% "scalacheck" % "1.13.5",
  "io.gatling" % "gatling-core" % "2.3.0",
  "io.gatling" % "gatling-http" % "2.3.0"
)
